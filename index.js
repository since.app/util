// Package imports
const nacl = require('tweetnacl-util')
const { randomBytes, secretbox: { keyLength } } = require('tweetnacl')

// Module imports
const crypt = require('./lib/crypt')
const is = require('./lib/is')
const str = require('./lib/str')

// Module exports common utility functions use in the Since.app project
module.exports = {
  crypt,
  is,
  nacl,
  str,
  generateKey () {
    return nacl.encodeBase64(randomBytes(keyLength))
  }
}
