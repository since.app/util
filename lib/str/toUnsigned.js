// Package imports
const util = require('tweetnacl-util')

/**
 * Converts a string into an array of unsigned 8-bit integers.
 *
 * Available encoding types: utf8, base64.
 *
 * @function toUnsigned
 * @param   {String}     str String to convert
 * @param   {String}     enc Character encoding(utf8 by default)
 * @returns {Uint8Array}     Array of unsigned integers
 */
module.exports = function toUnsigned (str, enc) {
  if (typeof str !== 'string') throw new Error('First argument must be a string')
  if (enc === 'utf8' || enc === undefined) return util.decodeUTF8(str)
  if (enc === 'base64') return util.decodeBase64(str)
  throw new Error('Unknown encoding type ' + enc)
}
