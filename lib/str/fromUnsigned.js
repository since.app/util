// Package imports
const util = require('tweetnacl-util')

/**
 * Converts an array of unsigned 8-bit integer into a string.
 *
 * Available encoding types: utf8, base64.
 *
 * @function fromUnsigned
 * @param   {Uint8Array} arr Array to convert
 * @param   {String}     enc Character encoding(utf8 by default)
 * @returns {String}         Resulting string
 */
module.exports = function fromUnsigned (arr, enc) {
  if (!(arr instanceof Uint8Array)) throw new Error('First argument must be a Uint8Array')
  if (enc === 'utf8' || enc === undefined) return util.encodeUTF8(arr)
  if (enc === 'base64') return util.encodeBase64(arr)
  throw new Error('Unknown encoding type ' + enc)
}
