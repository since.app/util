// Module imports
const base64 = require('./base64')
const decrypt = require('./decrypt')
const decrypto = require('./decrypto')
const encrypt = require('./encrypt')
const encrypto = require('./encrypto')
const hash = require('./hash')
const once = require('./once')
const verify = require('./verify')

// Module exports our cryptographic functions
module.exports = {
  base64,
  decrypt,
  decrypto,
  encrypt,
  encrypto,
  hash,
  once,
  verify
}
