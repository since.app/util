// Module imports
const encrypt = require('./encrypt')
const toUnsigned = require('../str/toUnsigned')

/**
 * Encrypts parts of or an entire object.
 *
 * @function encrypto
 * @param {Object}        obj   Object to encrypt
 * @param {String[]|true} props Properties to encrypt, true for all properties
 * @param {Uint8Array}    key   Encryption key
 */
module.exports = function encrypto (obj, props, key) {
  // Type check props
  if (props !== true && !Array.isArray(props)) {
    throw new Error('props must be `true` or an array of object properties as strings')
  }

  // DRY encrypt function
  const call = (prop) => {
    console.log('encrypting ' + prop)
    console.log(obj[prop])
    // Our encryption method takes in Uint8Array as data
    obj[prop] = typeof obj[prop] === 'string'
      ? encrypt(toUnsigned(obj[prop]), key)
      : obj[prop]
  }

  // Check if we're encrypting the entire object
  if (props === true) props = Object.keys(obj)

  // Encrypts the specified object properties
  props.forEach(call)
}
