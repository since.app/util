// Package imports
const { randomBytes } = require('crypto')

/**
 * Creates a set of random bytes and casts them to base64.
 *
 * @function          base64
 * @param    {Number} bytes  Number of bytes to generate
 * @returns  {String}        The resulting base64 string
 */
module.exports = function base64 (bytes) {
  return randomBytes(bytes).toString('base64')
}
