// Package imports
const bcrypt = require('bcrypt')

/**
 * Password hashing function.
 *
 * @async
 * @function hash
 * @param  {String} password Password to hash
 * @return {String}          Hashed password
 */
module.exports = async function hash (password) {
  return bcrypt.hash(password, 10)
}
