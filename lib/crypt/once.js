// Package imports
const crypto = require('crypto')

/**
 * Hashes a string(sha256).
 *
 * @param   {String} str String to hash
 * @returns {String}     Base64 result
 */
module.exports = function once (str) {
  return crypto.createHash('sha256').update(str).digest('base64')
}
