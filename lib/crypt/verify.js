// Package imports
const bcrypt = require('bcrypt')

/**
 * Password verification function.
 *
 * @async
 * @function verify
 * @param  {String} password Password to verify
 * @param  {String} hash     Hashed password to verify against
 * @return {Boolean}         Password is valid
 */
module.exports = async function verify (password, hash) {
  return bcrypt.compare(password, hash)
}
