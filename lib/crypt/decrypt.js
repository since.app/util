// Package imports
const { decodeBase64 } = require('tweetnacl-util')
const { secretbox: { nonceLength, open } } = require('tweetnacl')

/**
 * TweetNaCl decrypt function using unsigned 8-bit integer arrays.
 *
 * @function decrypt
 * @param  {String}     data Data to decrypt
 * @param  {Uint8Array} key  Encryption key
 * @return {Uint8Array}      Decrypted data as an unsigned 8-bit integer array
 * @see https://github.com/dchest/tweetnacl-js/wiki/Examples
 */
module.exports = function decrypt (data, key) {
  // Convert the received string into its Uint8Array instance
  const decoded = decodeBase64(data)

  // Extract the nonce from the decoded data
  const nonce = decoded.slice(0, nonceLength)

  // Extract the encrypted data
  const encrypted = decoded.slice(nonceLength, data.length)

  // Return the resulting unsigned integer array instead of converting it into a string
  return open(encrypted, nonce, key)
}
