// Module imports
const decrypt = require('./decrypt')
const fromUnsigned = require('../str/fromUnsigned')

/**
 * Decrypts parts of or an entire object.
 *
 * @function decrypto
 * @param {Object}        obj   Object to decrypt
 * @param {String[]|true} props Properties to decrypt, true for all properties
 * @param {Uint8Array}    key   Encryption key
 */
module.exports = function decrypto (obj, props, key) {
  // Type check props
  if (props !== true && !Array.isArray(props)) {
    throw new Error('props must be `true` or an array of object properties as strings')
  }

  // DRY encrypt property function
  const call = (prop) => {
    // Our decryption method a Uint8Array as data
    obj[prop] = typeof obj[prop] === 'string'
      ? fromUnsigned(decrypt(obj[prop], key))
      : obj[prop]
  }

  // Check if we're decrypting the entire object
  if (props === true) props = Object.keys(obj)

  // Decrypts the specified object properties
  props.forEach(call)
}
