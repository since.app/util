// Package imports
const { encodeBase64 } = require('tweetnacl-util')
const { randomBytes, secretbox } = require('tweetnacl')

/**
 * TweetNaCl encrypt function using unsigned 8-bit integer arrays.
 *
 * @function encrypt
 * @param  {Uint8Array} data Data to encrypt
 * @param  {Uint8Array} key  Encryption key
 * @return {String}          Encrypted data as a base64 string
 * @see https://github.com/dchest/tweetnacl-js/wiki/Examples
 */
module.exports = function encrypt (data, key) {
  // Nonce is randomized for each encryption call
  const nonce = randomBytes(secretbox.nonceLength)

  // Encrypt our data
  const box = secretbox(data, nonce, key)

  // Create the final output containing both nonce and the encrypted data
  const encrypted = new Uint8Array(nonce.length + box.length)

  // Set the nonce first thing in our output
  encrypted.set(nonce)

  // Add the encrypted data after the nonce
  encrypted.set(box, nonce.length)

  // Return the final output as a base64 encoded string
  return encodeBase64(encrypted)
}
